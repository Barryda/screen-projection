# Screen Projection

#### 介绍
安卓手机局域网投屏与反向控制应用程序

#### 系统框架
系统框架如下图所示，
手机服务端发起投屏，客户端负责接收，显示服务端的屏幕内容。
服务端连接USB外设后，授予USB调试权限后，客户端可控制服务端屏幕。

![输入图片说明](%E7%B3%BB%E7%BB%9F%E6%A1%86%E6%9E%B6.png)


#### 项目说明
1.  ScreenProjection：Android应用程序，使用TCP、Media Projection实现局域网投屏功能。
2.  STM32F4_ADB：移植ADB的USB通信协议，令STM32单片机能对Android手机进行ADB操作，实现反向控制的功能。
3.  libusb_adb：用libusb库移植ADB的USB通信功能，VS编译的桌面调试工具，主要用来调试反向控制的功能。

#### 安装教程
1.  手机安装ScreeProjection，需要Android5.0以上版本。
2.  STM32F4_ADB使用Keil μVision5编译环境。
3.  libusb_adb使用VS2019编译。

#### 使用说明
1.  手机服务端安装程序后，需要授权截屏，打开USB调试。
2.  服务端创建服务，客户端加入服务。
3.  服务端连接USB外设。
