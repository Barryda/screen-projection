#include "stm32f4xx.h"
#include "debug_usart.h"
#include "usbh_bsp.h"
#include "usbh_adb_core.h"

int main(void)
{
	Debug_USART_Config();
	
	USBH_Init(&USB_OTG_Core,
						USB_OTG_HS_CORE_ID,
						&USB_Host,
						&USBH_ADB_cb,
						&USR_cb);
	
	while(1)
	{
		USBH_Process(&USB_OTG_Core, &USB_Host);
	}
}
