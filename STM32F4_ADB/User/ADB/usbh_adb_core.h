/**
  ******************************************************************************
  * @file    usbh_adb_core.h
  * @author  Newtons_Prism
  * @version V1.0.0
  * @date    19-April-2022
  * @brief   USB ADB Library
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2022 Newtons_Prism</center></h2>
  *
  * Licensed under GNU General Public License v3.0, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        https://www.gnu.org/licenses/gpl-3.0
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


#ifndef __USBH_ADB_CORE_H
#define __USBH_ADB_CORE_H

#include "usbh_core.h"
#include "usbh_hcs.h"
#include "usbh_ioreq.h"
#include "usb_bsp.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* ADB通信数据量最大负载 */
#define MAX_PAYLOAD 255

/* ADB数据头标识符 */
#define A_SYNC 0x434e5953
#define A_CNXN 0x4e584e43
#define A_OPEN 0x4e45504f
#define A_OKAY 0x59414b4f
#define A_CLSE 0x45534c43
#define A_WRTE 0x45545257
#define A_AUTH 0x48545541

/* ADB版本，任意取值 */
#define A_VERSION 0x01

/* ADB AUTH KEY 参数 */
#define ADB_AUTH_RSAPUBLICKEY  0x03

/* ADB接口类 */
#define ADB_CLASS              0xff
#define ADB_SUBCLASS           0x42
#define ADB_PROTOCOL           0x01

static const char* AUTH_KEY = "ADB AUTH RSAPUBLICKEY";
static const char* TOUCH_CMD = "shell:while (true); do r=`logcat -t 1 | grep input`;  if [ $? -eq 0 ]; then echo $r; logcat -c; ${r#*@}; fi; done &";
//static const char* TOUCH_CMD = "while [[ true ]];do r=`logcat -t 1 | grep -E 'input|Killing'`;if [[ $? -eq 0 ]];then if [[ $r == *'screenprojection'* ]];then exit;fi;if [[ $r == *'@input'* ]];then ${r#*@};fi;logcat -c;fi;done &";

typedef struct amessage amessage;
typedef struct apacket apacket;
typedef enum ADB_State ADB_State;
typedef struct _ADB_Process ADB_Machine_TypeDef;

/* ADB数据头结构体 */
struct amessage {
	uint32_t command;   		//数据头标识符
	uint32_t arg0;      		//参数1   
	uint32_t arg1;      		//参数2    
	uint32_t data_length;   //数据体长度
	uint32_t data_check;    //数据体校验
	uint32_t magic;         //数据头校验
};

/* ADB数据包结构体 */
struct apacket {
	amessage msg;  							//数据头
	uint8_t data[MAX_PAYLOAD];  //数据体
};

/* ADB状态枚举 */
enum ADB_State {
  ADB_IDLE = 0, 	//空闲
	ADB_SEND_AUTH, 	//申请权限
	ADB_SEND_CMD,  	//发送命令行
  ADB_ERROR,     	//错误
};

/* Structure for ADB process */
struct _ADB_Process
{
  uint8_t   hc_num_in;   		//输入端点号
  uint8_t   hc_num_out;  		//输出端点号
	uint8_t   BulkInEp;				//输入端点
  uint8_t   BulkOutEp;  		//输出端点
  uint16_t  BulkInEpSize;	 	//输入端点大小
  uint16_t  BulkOutEpSize;	//输出端点大小
	uint16_t  zero_mask;			//零掩码
	ADB_State state;      		//ADB状态
}; 

/** @defgroup USBH_ADB_CORE_Exported_Variables
  * @{
  */ 
extern USBH_Class_cb_TypeDef  USBH_ADB_cb;
extern ADB_Machine_TypeDef    ADB_Machine;
/**
  * @}
  */ 

#endif
